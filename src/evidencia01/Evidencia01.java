/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package evidencia01;

/**
 *
 * @author SANDRA
 */
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
public class Evidencia01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String usuario="root";
        String password="Ricardo20240+-/*";
        String url="jdbc:mysql://localhost:3306/plataforma";
        Connection conexion;
        Statement statement;
        ResultSet rs;
            
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Evidencia01.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            conexion=DriverManager.getConnection(url, usuario, password);
            statement=conexion.createStatement();
            rs=statement.executeQuery("SELECT * FROM USUARIO");
            rs.next();
            do{
                System.out.println(rs.getInt("idusuario")+":"+ rs.getString("tipodocumento")+":"+rs.getString("nombres")+":"+rs.getString("apellidos")+":"+rs.getString("perfil")+":"+rs.getString("correo")+":"+rs.getString("usuariowindows")+":"+rs.getString("telefono"));
            }while (rs.next());
            
            //insert
            
            statement.executeUpdate("INSERT INTO USUARIO(idusuario, tipodocumento, nombres, apellidos, perfil, correo, usuariowindows, telefono) VALUES ('315247', 'CE', 'More Mu', 'Kita Uru', 'Usuario', 'mmkitau@dimo.com', 'mmkitau', '3117415856')");
            statement=conexion.createStatement();
            System.out.println(" ");
            rs=statement.executeQuery("SELECT * FROM USUARIO");
            rs.next();
            do{
                System.out.println(rs.getInt("idusuario")+":"+ rs.getString("tipodocumento")+":"+rs.getString("nombres")+":"+rs.getString("apellidos")+":"+rs.getString("perfil")+":"+rs.getString("correo")+":"+rs.getString("usuariowindows")+":"+rs.getString("telefono"));
            }while (rs.next());

            //UPDATE
            statement.executeUpdate("UPDATE usuario SET tipodocumento='CE', nombres='Luna Angelita', apellidos='Carmelita Sajon', perfil='Analista', correo='lacarmelitas@dimo.com', usuariowindows='lacarmelitas', telefono='3152147896' WHERE idusuario =35123456"); 
            statement=conexion.createStatement();
            System.out.println(" ");
            rs=statement.executeQuery("SELECT * FROM USUARIO");
            rs.next();
            do{
                System.out.println(rs.getInt("idusuario")+":"+ rs.getString("tipodocumento")+":"+rs.getString("nombres")+":"+rs.getString("apellidos")+":"+rs.getString("perfil")+":"+rs.getString("correo")+":"+rs.getString("usuariowindows")+":"+rs.getString("telefono"));
            }while (rs.next());
            
            //delete
            statement.executeUpdate("DELETE FROM usuario WHERE idusuario=40256397");
            statement=conexion.createStatement();
            System.out.println(" ");
            rs=statement.executeQuery("SELECT * FROM USUARIO");
            rs.next();
            do{
                System.out.println(rs.getInt("idusuario")+":"+ rs.getString("tipodocumento")+":"+rs.getString("nombres")+":"+rs.getString("apellidos")+":"+rs.getString("perfil")+":"+rs.getString("correo")+":"+rs.getString("usuariowindows")+":"+rs.getString("telefono"));
            }while (rs.next());
            
          

       
            
        } catch (SQLException ex) {
            Logger.getLogger(Evidencia01.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
